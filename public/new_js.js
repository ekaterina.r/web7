$(document).ready(function() {
    $('.multiple-items').slick({
		dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        arrows: true,
        responsive: [
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 2,
				slidesToScroll: 2,
              }
            }
        ]
    });
   
  }); 